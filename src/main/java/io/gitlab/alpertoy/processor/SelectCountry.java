package io.gitlab.alpertoy.processor;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SelectCountry implements Processor {
	
	// Country codes
	// Search by ISO 3166-1 2-letter or 3-letter country code
	List<String> countryList = Arrays.asList("us","gb","fr","es","ca");
	
    @Override
    public void process(Exchange exchange) throws Exception {
    	
    	// Process exchange by choosing a country randomly
    	
        Random random = new Random();
        String countryCode = countryList.get(random.nextInt(countryList.size()-1));
        log.info("selected Country code is " + countryCode);

        exchange.getIn().setHeader("countryId", countryCode);
    }
}
