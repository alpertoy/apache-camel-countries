package io.gitlab.alpertoy.routes;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.gitlab.alpertoy.processor.SelectCountry;

@Component
public class GetCountryRoute extends RouteBuilder {

	@Qualifier("dataSource")
	@Autowired
	private DataSource dataSource;

	@Autowired
	private SelectCountry selectCountry;

	@Override
	public void configure() throws Exception {

		onException(PSQLException.class).log(LoggingLevel.ERROR, "PSQLException in the route ${body}")
				.maximumRedeliveries(3).redeliveryDelay(3000).backOffMultiplier(2)
				.retryAttemptedLogLevel(LoggingLevel.ERROR);

		from("{{fromRoute}}").process(selectCountry).setHeader(Exchange.HTTP_METHOD, constant("GET"))
				.setHeader(Exchange.HTTP_URI, simple("https://restcountries.com/v2/alpha/${header.countryId}"))
				.to("https://restcountries.com/v2/alpha/us").convertBodyTo(String.class)
				.log("Restcountries API response is ${body}").removeHeader(Exchange.HTTP_URI)
				.setHeader(Exchange.HTTP_METHOD, constant("POST")).to("{{toRoute}}");
	}

}
