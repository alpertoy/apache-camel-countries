package io.gitlab.alpertoy.routes;

import javax.sql.DataSource;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.gson.GsonDataFormat;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.gitlab.alpertoy.domain.Country;
import io.gitlab.alpertoy.processor.InsertCountry;

@Component
public class PostCountryRoute extends RouteBuilder {
	
	@Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;
	
	@Autowired
	private InsertCountry insertCountry;

	@Override
	public void configure() throws Exception {
		onException(PSQLException.class).log(LoggingLevel.ERROR, "PSQLException in the route ${body}")
				.maximumRedeliveries(3).redeliveryDelay(3000).backOffMultiplier(2)
				.retryAttemptedLogLevel(LoggingLevel.ERROR);

		GsonDataFormat countryFormat = new GsonDataFormat(Country.class);

		from("restlet:http://localhost:8081/country?restletMethods=POST").routeId("countryPostRoute")
				.log("Received Body is ${body}").convertBodyTo(String.class).unmarshal(countryFormat)
				.log("Unmarshaled record is ${body}").process(insertCountry).to("{{dbNode}}").to("{{selectNode}}")
				.convertBodyTo(String.class).log("Inserted Country is ${body}");
	}

}
